<?php

require_once __DIR__ . '/vendor/autoload.php';

$class = new \UpsParser\Parser();

$single_result = $class->getParcelData('1Z71984Y7999782012');

dump($single_result);

$array_results = $class->getMultipleParcelData([
    '1Z71984Y7999782012', '1Z71984Y7996147580', '1Z71984Y7998976627',
    '1Z71984Y7996733035', '1Z71984Y7993222986', '1Z71984Y7993902198',
    '1Z71984Y7993255003', '1Z71984Y7992515037', '1Z71984Y7994922245',
    '1Z71984Y7993529477', '1Z71984Y7993247085'
]);

dump($array_results);

$generator = $class->getMultipleParcelDataGenerator([
    '1Z71984Y7999782012', '1Z71984Y7996147580', '1Z71984Y7998976627',
    '1Z71984Y7996733035', '1Z71984Y7993222986', '1Z71984Y7993902198',
    '1Z71984Y7993255003', '1Z71984Y7992515037', '1Z71984Y7994922245',
    '1Z71984Y7993529477', '1Z71984Y7993247085'
]);

foreach ($generator as $data) {
    dump($data);
}
