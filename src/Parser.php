<?php

namespace UpsParser;

class Parser
{
    const POST_URL = 'https://www.ups.com/track/api/Track/GetStatus?loc=en_DE';

    protected string $cookie = '';
    protected string $xsrf = '';

    public function __construct()
    {
        $this->setCookies();
    }

    /**
     * @param $id
     * @return array
     */
    public function getParcelData($id): array
    {
        try {
            $data = json_decode($this->getJsonData($id));
            $details = $data->trackDetails[0];
            $tracking_data = array_map(function ($item) {
                return [
                    'status' => trim($item->activityScan),
                    'date' => $item->date . ' ' . $item->time
                ];
            }, $details->shipmentProgressActivities);
            return [
                'id' => $id,
                'status' => (int) $data->statusCode,
                'message' => $data->statusText,
                'data' => $tracking_data
            ];
        } catch (\Exception $exception) {
            return [
                'id' => $id,
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getMultipleParcelData(array $ids): array
    {
        $results = [];

        foreach ($ids as $id) {
            $results[$id] = $this->getParcelData($id);
        }

        return $results;
    }

    /**
     * @param array $ids
     * @return \Generator
     */
    public function getMultipleParcelDataGenerator(array $ids): \Generator
    {
        foreach ($ids as $id) {
            yield $this->getParcelData($id);
        }
    }

    /**
     * @param $id
     * @return bool|string
     */
    protected function getJsonData($id)
    {
        $handle = curl_init(self::POST_URL);

        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Accept: application/json',
            'User-Agent: Chrome',
            "Cookie: $this->cookie",
            "X-XSRF-TOKEN: $this->xsrf",
        ]);

        curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode(['TrackingNumber' => [$id]]));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($handle);
        $status = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if ($status === 403 || $status === 401) {
            $this->setCookies();
            return $this->getJsonData($id);
        }

        return $result;
    }

    /**
     * Set cookies for parsing data
     */
    protected function setCookies(): void
    {
        $handle = curl_init('https://www.ups.com/track?loc=de_DE&tracknum=1Z71984Y7996147580&requester=WT/trackdetails');
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_HEADER, 1);

        $response = curl_exec($handle);

        $header_size = curl_getinfo($handle, CURLINFO_HEADER_SIZE);
        $headers = substr($response, 0, $header_size);

        $cookies = [];
        foreach (explode("\r\n", $headers) as $header) {
            $key = explode(':', $header)[0];
            if ($key === 'Set-Cookie') {
                $cookies[] = explode(';', explode(':', $header)[1])[0];
            }
        }

        $this->setXsrf($cookies);

        $this->cookie = implode('; ', $cookies);
    }

    /**
     * Set xsrf to property from cookies
     *
     * @param array $cookies
     */
    protected function setXsrf(array $cookies): void
    {
        foreach ($cookies as $cookie) {
            $cookie_exploded = explode('=', $cookie);

            if (trim($cookie_exploded[0]) === 'X-XSRF-TOKEN-ST') {
                $this->xsrf = $cookie_exploded[1];
            }
        }
    }
}
